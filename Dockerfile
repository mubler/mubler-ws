FROM openjdk:8-jdk-alpine

VOLUME /tmp
COPY target/mubler-ws.jar .
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/mubler-ws.jar"]